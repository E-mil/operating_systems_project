#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include "buddymemalloc.h"

#define MAX_ORDER 25

typedef struct memory_block memory_block;

struct memory_block {
	memory_block* next_block;
	char* data;
	size_t size;
	char allocated;
	size_t order;
};


static memory_block* blocks[MAX_ORDER + 1];
char* start = NULL;

static memory_block* merge_buddies(memory_block* block) {
	memory_block* buddy = (memory_block*)(start + (((char*)block - start) ^ block->size));
	while (block->order != MAX_ORDER && buddy->allocated == 0 && buddy->size == block->size) {
		memory_block* prev = NULL;
		memory_block* crt = blocks[block->order];
		while (crt != NULL) {
			if (crt == block || crt == buddy) {
				if (prev == NULL) {
					blocks[block->order] = crt->next_block;
				} else {
					prev->next_block = crt->next_block;
				}
			} else {
				prev = crt;
			}
			crt = crt->next_block;
		}
		if (block > buddy) {
			block = buddy;
		}
		block->size = block->size << 1;
		block->order++;
		buddy = (memory_block*)(start + (((char*)block - start) ^ block->size));
	}
	return block;
}

void free(void *ptr) {
	if (ptr != NULL) {
		memory_block* block = (memory_block*)((char*)ptr - sizeof(memory_block));
		if (block->allocated == 0) {
			fprintf(stderr, "%s\n", "Error: Double Free ;)");
			exit(EXIT_FAILURE);
		} else {
			block->allocated = 0;
			block = merge_buddies(block);
			block->next_block = blocks[block->order];
			blocks[block->order] = block;
		}
	}
}

void *malloc(size_t size) {
	if (size == 0) {
		return NULL;
	}

	if (start == NULL) {
		int i;
		for (i = 0; i < MAX_ORDER; i++) {
			blocks[i] = NULL;
		}
		start = sbrk(1 << MAX_ORDER);
		if (start < 0) {
			return NULL;
		}
		blocks[MAX_ORDER] = (memory_block*)start;
		blocks[MAX_ORDER]->next_block = NULL;
		blocks[MAX_ORDER]->size = (1 << MAX_ORDER);
		blocks[MAX_ORDER]->order = MAX_ORDER;
		blocks[MAX_ORDER]->allocated = 0;
		blocks[MAX_ORDER]->data = start + sizeof(memory_block);
	}

	size_t order = 0;
	while ((1 << order) < (size + sizeof(memory_block)))
		order++;

	if (blocks[order] != NULL) {
		memory_block* block = blocks[order];
		blocks[order] = block->next_block;
		block->next_block = NULL;
		block->allocated = 1;
		return block->data;
	} else {
		int free_order;
		for (free_order = order + 1; free_order < MAX_ORDER + 1; free_order++) {
			if (blocks[free_order] != NULL) {
				while (free_order > order) {
					memory_block* block = blocks[free_order];
					blocks[free_order] = block->next_block;
					block->size = block->size >> 1;
					block->order--;
					memory_block* new_block = (memory_block*)((char*)block + block->size);
					new_block->size = block->size;
					new_block->order = block->order;
					new_block->allocated = 0;
					new_block->data = (char*)new_block + sizeof(memory_block);
					block->next_block = new_block;
					new_block->next_block = NULL;
					free_order--;
					blocks[free_order] = block;
				}
				memory_block* block = blocks[order];
				blocks[order] = block->next_block;
				block->next_block = NULL;
				block->allocated = 1;
				return block->data;
			}
		}
	}
	//set errno?
	return NULL;
}

void *realloc(void *ptr, size_t size) {
	if (size == 0) {
		free(ptr);
		return NULL;
	}
	if (ptr == NULL) {
		return malloc(size);
	}

	char* new_ptr = malloc(size);
	if (new_ptr == NULL)
		return NULL;
	memory_block* block = (memory_block*)((char*)ptr - sizeof(memory_block));
	int i;
	for (i = 0; i < block->size && i < size; i++) {
		*(new_ptr + i) = *((char*)ptr + i);
	}
	free(ptr);
	return new_ptr;
}

void *calloc(size_t nitems, size_t size) {
	size_t actual_size = nitems * size;
	if (actual_size == 0) {
		return NULL;
	}
	char* ptr = malloc(actual_size);
	if (ptr == NULL)
		return NULL;
	int i;
	for (i = 0; i < actual_size; i++) {
		ptr[i] = 0;
	}
	return ptr;
}
