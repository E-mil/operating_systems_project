#ifndef _LLMALLOC
#define _LLMALLOC 1

#include <stddef.h>

void free(void *ptr);

void *malloc(size_t size);

void *realloc(void *ptr, size_t size);

void *calloc(size_t nitems, size_t size);

#endif
