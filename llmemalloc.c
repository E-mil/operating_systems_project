#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include "llmemalloc.h"

typedef struct memory_block memory_block;

struct memory_block {
	memory_block* next_block;
	memory_block* previous_block;
	char* ptr;
	size_t size;
	char allocated;
};

static memory_block* first_block = NULL;
static memory_block* last_block = NULL;

static void add_memory_block(memory_block* block) {
	if (first_block == NULL) {
		first_block = block;
		last_block = block;
		block->next_block = NULL;
		block->previous_block = NULL;
	} else {
		block->next_block = NULL;
		block->previous_block = last_block;
		last_block->next_block = block;
		last_block = block;
	}
}

static void split_block(memory_block* block, size_t size) {
	if (block->size > size + sizeof(memory_block)) {
		memory_block* new_block = (memory_block*)((char*)block + sizeof(memory_block) + size);
		new_block->size = block->size - size - sizeof(memory_block);
		new_block->allocated = 0;
		new_block->ptr = (char*)new_block + sizeof(memory_block);
		new_block->next_block = block->next_block;
		new_block->previous_block = block;
		if (block->next_block != NULL) {
			block->next_block->previous_block = new_block;
		} else {
			last_block = new_block;
		}
		block->next_block = new_block;
		block->size = size;
	}
}

static memory_block* merge_blocks(memory_block* bottom, memory_block* top) {
	bottom->next_block = top->next_block;
	if (top->next_block != NULL) {
		top->next_block->previous_block = bottom;
	}
	bottom->size += top->size + sizeof(memory_block);
	if (top == last_block) {
		last_block = bottom;
	}
	return bottom;
}

void free(void *ptr) {
	if (ptr != NULL) {
		memory_block* block = (memory_block*)((char*)ptr - sizeof(memory_block));
		if (block->allocated == 0) {
			fprintf(stderr, "%s\n", "Error: Double Free ;)");
			exit(EXIT_FAILURE);
		} else {
			block->allocated = 0;
			if (block->next_block != NULL && block->next_block->allocated == 0) {
				block = merge_blocks(block, block->next_block);
			}
			if (block->previous_block != NULL && block->previous_block->allocated == 0) {
				block = merge_blocks(block->previous_block, block);
			}
		}
	}
}

void *malloc(size_t size) {
	if (size == 0) {
		return NULL;
	}

	memory_block *current_block = first_block;
	while (current_block != NULL) {
		if (current_block->allocated == 0 && current_block->size >= size) {
			current_block->allocated = 1;
			split_block(current_block, size);
			return current_block->ptr;
		}
		current_block = current_block->next_block;
	}

	memory_block *new_block = sbrk(sizeof(memory_block) + size);
	if (new_block < 0)
		return NULL;
	new_block->ptr = (char*)new_block + sizeof(memory_block);
	new_block->allocated = 1;
	new_block->size = size;
	add_memory_block(new_block);
	return new_block->ptr;
}

void *realloc(void *ptr, size_t size) {
	if (size == 0) {
		free(ptr);
		return NULL;
	}
	if (ptr == NULL) {
		return malloc(size);
	}

	char* new_ptr = malloc(size);
	if (new_ptr == NULL)
		return NULL;
	memory_block* block = (memory_block*)((char*)ptr - sizeof(memory_block));
	int i;
	for (i = 0; i < block->size && i < size; i++) {
		*(new_ptr + i) = *((char*)ptr + i);
	}
	free(ptr);
	return new_ptr;
}

void *calloc(size_t nitems, size_t size) {
	size_t actual_size = nitems * size;
	if (actual_size == 0) {
		return NULL;
	}
	char* ptr = malloc(actual_size);
	if (ptr == NULL)
		return NULL;
	int i;
	for (i = 0; i < actual_size; i++) {
		ptr[i] = 0;
	}
	return ptr;
}
